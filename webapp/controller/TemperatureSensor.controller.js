sap.ui.define([
	"mosbach/dhbw/i40/visualization/controller/BaseController"
], function(BaseController) {
	"use strict";

	return BaseController.extend("mosbach.dhbw.i40.visualization.controller.TemperatureSensor", {

		_intervalID: null,

		_getSensorData: function(oController) {
			var odataModel = oController._getModel("odata");
			var temperatureModel = oController._getModel("temperature");
			var timeNow = new Date();
			var intervalMinute = Number(temperatureModel.getProperty("/intervalMinute"));
			var intervalHour = Number(temperatureModel.getProperty("/intervalHour"));
			//(minutes * seconds/minute)+hour*seconds/hour)*milliseconds/second
			var timeStart = new Date(timeNow.getTime() - (intervalMinute * 60 + intervalHour * 3600) * 1000);

			odataModel.read("/T_IOT_D56422AD5C91A97CE851/", {
				filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", timeStart, timeNow)],
				sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", true)],
				success: function(oData) {
					if (oData.results.length > 0) {
						var oMinMaxAverage = oController._calculateMinMaxAverage(oData.results);
						temperatureModel.setProperty("/current", oMinMaxAverage.current);
						temperatureModel.setProperty("/minimum", oMinMaxAverage.minimum);
						temperatureModel.setProperty("/maximum", oMinMaxAverage.maximum);
						temperatureModel.setProperty("/average", oMinMaxAverage.average);
						oController._checkTemperature("current");
						oController._checkTemperature("average");
						oController._checkTemperature("minimum");
						oController._checkTemperature("maximum");
						oController._isCriticalTemperature(oData.results[0]);
					} else {
						oController._resetTileContentColor("current");
						oController._resetTileContentColor("average");
						oController._resetTileContentColor("minimum");
						oController._resetTileContentColor("maximum");
					}
				}
			});
		},
		
		onAfterRendering: function() {
			if (this._intervalID === null) {
				var oController = this;
				this.getView().byId("refreshRateInput").getBinding("value").attachChange(function() {
					if (!oController._getModel("temperature").getProperty("/criticalNotification")) {
						oController.clearInt();
						oController.restartInterval();
					}
				});
				this._getSensorData(this);
				this._intervalID = setInterval(this._getSensorData, this._getModel("temperature").getProperty("/refreshRate") * 1000, this);
			}
		},

		restartInterval: function() {
			this._getSensorData(this);
			this._intervalID = setInterval(this._getSensorData, this._getModel("temperature").getProperty("/refreshRate") * 1000, this);
		},

		clearInt: function() {
			clearInterval(this._intervalID);
		},

		onTabSelected: function(oEvent) {

			if (!this._getModel("temperature").getProperty("/criticalNotification")) {
				switch (oEvent.getParameters("key").key) {
					case "1":
						this.restartInterval();
						break;
					default:
						this.clearInt();
				}
			} else {
				if (oEvent.getParameters("source").key === "3") {
					this.clearInt();
					this.restartInterval();
				}
			}
		},

		onShowData: function() {

			var oResourceBundle = this._getResourceBundle();

			var oIntervalStart = this.getView().byId("dtpIntervalStart").getDateValue();
			var oIntervalEnd = this.getView().byId("dtpIntervalEnd").getDateValue();

			if (oIntervalStart > oIntervalEnd || oIntervalStart === null || oIntervalEnd === null) {
				var dialog = new sap.m.Dialog({
					title: oResourceBundle.getText("wrongTimeInterval"),
					type: 'Message',
					state: 'Error',
					content: new sap.m.Text({
						text: oResourceBundle.getText("wrongTimeIntervalMessage")
					}),
					beginButton: new sap.m.Button({
						text: oResourceBundle.getText("ok"),
						press: function() {
							dialog.close();
						}
					}),
					afterClose: function() {
						dialog.destroy();
					}
				});

				dialog.open();
				return;
			}

			var oVizFrame = this.getView().byId("idcolumn");
			oVizFrame.destroyDataset();
			oVizFrame.destroyFeeds();

			var oModel = this.getView().getModel("odata");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: oResourceBundle.getText("time"),
					value: {
						path: "C_TIMESTAMP",
						formatter: function(sValue) {
							var timeFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
								pattern: "dd.MM. HH:mm:ss"
							});
							return timeFormat.format(sValue);
						}
					}
				}],

				measures: [{
					name: oResourceBundle.getText("temperature"),
					value: "{C_VALUE}"
				}],

				data: {
					path: "odata>/T_IOT_D56422AD5C91A97CE851",
					filters: [new sap.ui.model.Filter("C_TIMESTAMP", "BT", oIntervalStart, oIntervalEnd)],
					sorters: [new sap.ui.model.Sorter("C_TIMESTAMP", true)]
				}
			});

			oVizFrame.setDataset(oDataset);
			oVizFrame.setModel(oModel);
			oVizFrame.setVizType('line');

			//                4.Set Viz properties
			oVizFrame.setVizProperties({
				plotArea: {
					colorPalette: d3.scale.category20().range()
				},
				title: {
					text: oResourceBundle.getText("temperatureDiagramTitle")
				}

			});

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [oResourceBundle.getText("temperature")]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': [oResourceBundle.getText("time")]
				});
			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
		},

		_calculateMinMaxAverage: function(measurements) {

			if (measurements.length === 0) {
				return {
					current: 0.0,
					minimum: 0.0,
					maximum: 0.0,
					average: 0.0
				};
			}
			var min = Number.MAX_VALUE;
			var max = Number.MIN_VALUE;
			var sum = 0;
			var count = 0;

			jQuery.each(measurements, function(index, measurement) {
				var value = Number(measurement.C_VALUE);
				if (value < min) min = value;
				if (value > max) max = value;
				sum += value;
				count += 1;
			});

			return {
				current: Number(measurements[0].C_VALUE),
				minimum: min,
				maximum: max,
				average: sum / count
			};
		},
		_checkTemperature: function(sType) {
			var oTemperatureModel = this._getModel("temperature");
			var nAllowedMinTemp = Number(oTemperatureModel.getProperty("/allowedMin"));
			var nAllowedMaxTemp = Number(oTemperatureModel.getProperty("/allowedMax"));
			var nTemp = Number(oTemperatureModel.getProperty("/" + sType));
			var nCritical = (nAllowedMaxTemp - nAllowedMinTemp) / 10;

			var tileId = sType + "TemperatureTile";

			if (nTemp > nAllowedMaxTemp || nTemp < nAllowedMinTemp) {
				this.byId(tileId).setValueColor("Error");
				return;
			}

			if ((nAllowedMaxTemp - nTemp) < nCritical || (nTemp - nAllowedMinTemp) < nCritical) {
				this.byId(tileId).setValueColor("Critical");
				return;
			}
			this.byId(tileId).setValueColor("Good");
		},

		_resetTileContentColor: function(sType) {

			var oTileContent = this.byId(sType + "TemperatureTile");
			oTileContent.setValueColor("Neutral");
			oTileContent.setValue(0.0);
		},

		_isCriticalTemperature: function(oCurrentValue) {
			var oTemperatureModel = this._getModel("temperature");
			var nAllowedMinTemp = Number(oTemperatureModel.getProperty("/allowedMin"));
			var nAllowedMaxTemp = Number(oTemperatureModel.getProperty("/allowedMax"));
			var nCurrentTemp = Number(oCurrentValue.C_VALUE);
			var bNotificationSelected = oTemperatureModel.getProperty("/criticalNotification");
			
			if ((nCurrentTemp < nAllowedMinTemp || nCurrentTemp > nAllowedMaxTemp) && bNotificationSelected) {
				emailjs.send("gmail", "notificationtemplate", {
					receiver: oTemperatureModel.getProperty("/email"),
					timestamp: sap.ui.core.format.DateFormat.getDateTimeInstance({
						style: "long"
					}).format(oCurrentValue.C_TIMESTAMP),
					sensor: "Temperatursensor",
					value: nCurrentTemp
				});
			}
		},

		onEmailChanged: function(oEvent) {
			this._getModel("temperature").setProperty("/email", oEvent.getParameters().value);
		}
	});
});